<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>バイクサブスクTOP</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a href="MotoList" class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
            <c:if test="${userInfo.id == null}">
            	<a href="Login" class="btn bg-success mr-2" type="button" value="ログイン">ログイン</a>
            	<a href="Signup" class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
            </c:if>
            <c:if test="${userInfo.id != null}">
            	<a href="Logout" class="btn bg-success mr-2" type="button" value="ログアウト">ログアウト</a>
            </c:if>
        </div>
    </header>
    <div class=" container text-center p-2 mb-3 bg-danger">
        <div class="top mb-2" >
            <h2 class="pb-3">バイクサブスク</h2>
            <p>バイクのサブスクリプションサイト、１か月からレンタル可能!!</p>
            <a href="Signup" class="mr-2 bg-primary btn" type="button" value="新規登録">新規登録</a>
            <c:if test="${userInfo.id == null }">
            	<span class="mr-2">or</span>
            	<a href="Login" class="btn bg-success" type="button" value="ログイン">ログイン</a>
            </c:if>
        </div>
        <div class="middle mb-3">
            <h2>バイク一覧</h2>
            <div class="moto">
                <a href="MotoList" class="btn bg-warning" type="button" value="バイク一覧">バイク一覧</a>
            </div>
        </div>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="MotoList">バイク一覧</a></li>
                <li><a class="text-dark" href="Login">ログイン</a></li>
                <li><a class="text-dark" href="Signup">新規登録</a> </li>
            </ul>
        </div>
    </footer>

</body>
</html>