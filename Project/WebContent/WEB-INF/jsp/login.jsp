<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ログイン</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
            <a class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
        </div>
    </header>
    <div class=" bg-secondary container text-center mx-2">
        <h2 class="mb-4">ログイン画面</h2>
        <c:if test="${ errMsg != null}">
			<h3 class="alert alert-light text-danger" role="alert">
  				${errMsg}
			</h3>
		</c:if>
        <form method="post" action="Login">
            <div class="loginId mb-4">
                    <span class="mr-4">ログインID</span>
                    <input type="text" name="loginId">
            </div>
            <div class="password mb-4">
                    <span class="mr-4">パスワード</span>
                    <input type="password" name="password">
            </div>
            <div class="btn">
                <input class="bg-primary" type="submit" value="ログイン">
            </div>
        </form>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>