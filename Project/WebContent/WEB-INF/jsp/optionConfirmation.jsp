<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>オプション確認</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>

    </style>
</head>

<body>
    <header class="bg-info pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a href="MotoList" class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
            <c:if test="${userInfo.id == null}">
            	<a href="Login" class="btn bg-success mr-2" type="button" value="ログイン">ログイン</a>
            	<a href="Signup" class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
            </c:if>
            <c:if test="${userInfo.id != null}">
            	<a href="Logout" class="btn bg-success mr-2" type="button" value="ログアウト">ログアウト</a>
            </c:if>
        </div>
    </header>
    <h3 class="text-center">オプション、期間選択</h3>
    <div class="period bg-success container text-center my-2 p-2">
        <p>レンタル期間：1か月</p>
    </div>
    <div class="option-confirmation bg-danger container text-center my-2 p-2">
    	<c:forEach var="option" items="${optionList}">
    		<c:forEach var="choice" items="${choiceOption}">
    			<c:if test="${choice.id == option_id">
        			<p>${option.name}：<select name="example">
                		<option name="select" value="${option.id}">あり</option>
                		<option name="delete" value="${option.id}">なし</option>
            		</select></p>
            	</c:if>
     			<c:if test="${choice.id == null}">
            		<p>${option.name}：<select name="example">
                		<option name="delete" value="${option.id}">なし</option>
                		<option name="select" value="${option.id}">あり</option>
            		</select></p>
            	</c:if>

        	</c:forEach>
        </c:forEach>
        <!--  <p>ランプ：<select name="example">
                <option value="ランプあり">あり</option>
                <option value="ランプなし">なし</option>
            </select></p>
        <p>バッグ：<select name="example">
                <option value="なし">なし</option>
                <option value="タンクバッグ">タンクバッグ</option>
                <option value="サイドバッグ">サイドバッグ</option>
            </select></p>
        <p>ETC：<select name="example">
                <option value="ETCあり">あり</option>
                <option value="ETCなし">なし</option>
            </select></p>
            -->
        <a href="#" class="btn btn-light">戻る</a>
        <a href="ContractConfarmation" class="btn btn-primary">次へ</a>
    </div>

    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>メニュー</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body></html>