<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ログアウト</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        .container{
            width:1000px;
            height:200px;
        }
    </style>
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
        </div>
    </header>
    <div class=" bg-secondary text-center mb-2 container">
        <h2 class="mb-4 py-3">ログアウト画面</h2>
        <form method="post" actoin="Logout">
            <div class="row" >
                <div class="cansel col-sm-6">
                	<a href="Index" class="btn">戻る</a>
                   <!--  <input type="submit" value="戻る">   -->
                </div>
                <div class="logout col-sm-6">
                    <input type="submit" name="logout" class="btn btn-danger" value="ログアウト">
                </div>
            </div>
        </form>
    </div>
    <footer class="bg-warning">
        <div class="ml-2" Style="height:110px;">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>