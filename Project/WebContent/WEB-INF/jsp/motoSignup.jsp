<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>バイク新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        .container{
            width:1000px;
            margin:0 auto;

        }
    </style>
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
            <a class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
        </div>
    </header>
    <div class=" bg-secondary container text-center">
        <h2 class="mb-4">新規登録画面</h2>
        <form>
            <div class="moto_id">

            </div>
            <div class="name mb-4">
                    <span class="mr-4">名前</span>
                    <input type="text"　name="name">
            </div>
            <div class="img mb-4">
                    <span class="mr-4">写真</span>
                    <input type="file" name="img">
            </div>
            <div class="maker mb-4">
                    <span class="mr-4">メーカー</span>
                    <input type="text" name="maker">
            </div>
            <div class="gas mb-4">
                    <span class="mr-4">排気量</span>
                    <input type="text" name="gas">
            </div>
            <div class="price mb-4">
                    <span class="mr-4">レンタル価格</span>
                    <input type="text" name="price">
            </div>
            <div class="maker_explain mb-4">
                   <span class="mr-4">メーカー説明</span>
                　　<textarea name="maker_explain" cols="30" rows="2"></textarea>
            </div>
            <div class="licence">
                <span class="mr-4">必須免許証</span>
                <select name="licence">
                    <option value="原付免許">原付免許</option>
                    <option value="小型二輪免許">小型二輪免許</option>
                    <option value="AT限定小型二輪免許">AT限定小型二輪免許</option>
                    <option value="普通自動二輪免許">普通自動二輪免許</option>
                    <option value="AT限定普通自動二輪免許">AT限定普通自動二輪免許</option>
                    <option value="大型自動二輪免許">大型自動二輪免許</option>
                    <option value="AT限定大型自動二輪免許">AT限定大型自動二輪免許</option>
                    <option value="普通自動車免許">普通自動車免許</option>
                </select>
            </div>
            <div class="date">
                <input type="hidden" name="createDate">
                <input type="hidden" name="updateDate">
            </div>
            <div class="btn">
                <input class="bg-primary" type="button" value="更新">
            </div>
        </form>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>