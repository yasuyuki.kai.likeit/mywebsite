<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>契約履歴情報</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>

    </style>
</head>
<body>
    <header class="bg-info pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn btn btn-primary mr-2" value="バイク一覧">バイク一覧</a>
            <a class="btn bg-warning mr-2" value="ログイン">ログイン</a>
            <a class="btn bg-primary mr-2" value="新規登録">新規登録</a>
        </div>
    </header>
    <div class="moto container text-center my-2 p-2">
        <h1 class="mt-2 py-2">390DUKE</h1>
        <a href="#"><img src="img/390DUKE.png"></a>
    </div>
    <div class="period bg-success container text-center my-2 p-2">
        <p>レンタル期間：1か月</p>
    </div>
    <div class="option-confirmation bg-danger container text-center my-2 p-2">
        <p>マフラー：マフラー１ 料金：2500円</p>
        <p>ランプ：なし</p>
        <p>バッグ：サイドバッグ　料金：2000円</p>
        <p>ETC：なし</p>
    </div>
    <div class="total-price bg-info container text-center my-2 p-2">
        <p>DUKE390：月 60,000円</p>
        <p>オプション：月 4,500円</p>
        <p>保険料：月 50,000円</p>
        <p>総合計額： 114,500円</p>
        <p>月々の金額：月 114,500円</p>
    </div>


    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>メニュー</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>