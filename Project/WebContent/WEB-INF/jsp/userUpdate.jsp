<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザー情報編集</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        .container{
            width:1000px;
            margin:0 auto;

        }
    </style>
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
        </div>
    </header>
    <div class=" bg-secondary container text-center">
        <h2 class="mb-4">ユーザー情報編集</h2>
        <form>
            <div class="loginID mb-4">
                    <span class="mr-4">ログインID</span>
                    <input type="text" value="1"　name="loginId" readonly>
            </div>
            <div class="password mb-4">
                    <span class="mr-4">パスワード</span>
                    <input type="password" name="password">
            </div>
            <div class="password mb-4">
                    <span class="mr-4">パスワード(確認)</span>
                    <input type="password" name="rePassword">
            </div>
            <div class="user-name mb-4">
                    <span class="mr-4">ユーザー名</span>
                    <input type="text" name="name" value="kai">
            </div>
            <div class="licence mb-4">
                    <p class="mr-4">免許証 （複数選択可）：
                    <input type="checkbox" name="licence" value="scooters">原付免許（原付１種）
                    <input type="checkbox" name="licence" value="smMoto">小型自動二輪免許
                    <input type="checkbox" name="licence" value="AtSmMoto">AT限定小型自動二輪免許
                    <input type="checkbox" name="licence" value="ordinaryMoto">普通自動二輪免許
                    <input type="checkbox" name="licence" value="AtOrsinaryMoto">AT限定自動二輪免許
                    <input type="checkbox" name="licence" value="lgMoto">大型自動二輪免許
                    <input type="checkbox" name="licence" value="AtLgMoto">AT限定自動二輪免許
                    <input type="checkbox" name="licence" value="car">普通自動車免許
                    </p>
            </div>
            <div class="date">
                <input type="hidden" name="createDate">
                <input type="hidden" name="updateDate">
            </div>
            <div class="btn">
                <input class="bg-primary" type="button" value="更新">
            </div>
        </form>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>