<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>オプション一覧</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a href="MotoList" class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
            <c:if test="${userInfo.id == null}">
            	<a href="Login" class="btn bg-success mr-2" type="button" value="ログイン">ログイン</a>
            	<a href="Signup" class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
            </c:if>
            <c:if test="${userInfo.id != null}">
            	<a href="Logout" class="btn bg-success mr-2" type="button" value="ログアウト">ログアウト</a>
            </c:if>
        </div>
    </header>

    <div class=" bg-primary container text-center my-2 p-2">
        <form method="post" action="OptionList">
        <h2 class="mb-4">レンタル期間</h2>
        	<c:if test="${ errMsg != null}">
				<h3 class="alert alert-light text-danger" role="alert">
  					${errMsg}
				</h3>
			</c:if>

            <div class="period">
                <div class="form-group">
                	<c:forEach var="rental" items="${rentalList}">
                    	<div class="form-check form-check-inline">
                        	<input type="radio" value="${retal.id}" name="period" class="form-check-input"/>
                        	<label class="form-check-label" for="period">${rental.period}</label>
                    	</div>
                    </c:forEach>
                </div>
            </div>
		<c:if test="${optionList !=  null}">
        <h2>オプション一覧</h2>
        <div class="contents form-group">
            <div class="content">

                <c:forEach var="option" items="${optionList}">
                	<div class="from-check form-check-inline">
                    	<input type="checkbox" value="${option.id}" name="option" class="form-check-input">
                    	<label class="from-check-label" for="maffler1">${option.name}　料金：${option.price}</label>
                	</div>
                </c:forEach>

            </div>
           <!--  <div class="content lanp">
                <label for="lanp" class="control-label col-sm-2">ランプ</label>
                <div class="from-check form-check-inline">
                    <input type="checkbox" value="2" name="lanp" class="form-check-input">
                    <label class="form-check-label" for="lanp">LED　料金：1000円</label>
                </div>
            </div>
            <div class="content bag">
                <label for="bag" class="control-label col-sm-2">バッグ</label>
                <div class="from-check form-check-inline">
                    <input type="checkbox" value="5" name="bag" class="form-check-input">
                    <label class="form-check-label" for="bag1">タンクバッグ　料金：1500円</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="checkbox" value="4 name="bag" class="form-check-input">
                    <label class="form-check-label" for="bag2">サイドバック　料金：2000円</label>
                </div>
            </div>
            <div class="content etc">
                <label for="etc" class="control-label col-sm-2">ETC</label>
                <div class="form-check form-check-inline">
                    <input type="checkbox" value="3 name="etc" class="form-check-input">
                    <label class="form-check-label" for="etc">ETC　料金：2000円</label>
                </div>
            </div> -->
        </div>
        </c:if>
        <div class="next">
            <input type="submit" value="次へ">
        </div>
       </form>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>