<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>バイク一覧</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        .moto{
            width: 250px;
            border: solid 1px red;
        }
    </style>
</head>
<body>
    <header class="pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <c:if test="${userInfo.id == null}">
            	<a href="Login" class="btn bg-success mr-2" type="button" value="ログイン">ログイン</a>
            	<a href="Signup" class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
            </c:if>
            <c:if test="${userInfo.id != null}">
            	<a href="Logout" class="btn bg-success mr-2" type="button" value="ログアウト">ログアウト</a>
            </c:if>
        </div>
    </header>
    <div class="moto-list text-center mx-2 p-2">
        <div class="moto-sreach container border border-primary">
            <h3 mb-2>検索</h3>
            <form method="post" action="MotoList">

                 <div class="form-group">
                  <!--<label for="name" class="control-label col-sm-2">名前</label>
                  <div>
                    <input type="text" name="moto-name" id="moto-name" class="form-control"/>
                  </div>
                </div> -->

                <div class="form-group">
                  	<label for="makers" class="control-label col-sm-2">メーカー</label>
                  	<c:forEach var="maker" items="${makerList}">
                  	<div class="form-check form-check-inline">
                   		 <input type="checkbox" value="${maker.id}" name="maker" class="form-check-input"/>
                    	 <label class="form-check-label" for="maker1">${maker.name}</label>
                  	</div>
                  </c:forEach>

                 <!-- <div class="form-check form-check-inline">
                    <input type="checkbox" name="maker" class="form-check-input"/>
                    <label class="form-check-label" for="maker2">YAMAHA</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input type="checkbox" name="maker" class="form-check-input"/>
                    <label class="form-check-label" for="maker3">Kawsaki</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input type="checkbox" name="maker" class="form-check-input"/>
                    <label class="form-check-label" for="maker4">KTM</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input type="checkbox" name="maker" class="form-check-input"/>
                    <label class="form-check-label" for="maker5">BRP</label>
                  </div>
                    <div class="form-check form-check-inline">
                    <input type="checkbox" name="maker" class="form-check-input"/>
                    <label class="form-check-label" for="maker6">SUZUKI</label>
                  </div>
                   -->
                </div>
             <!--    <div class="form-group">
                    <label for="gas" class="control-label col-sm-2">排気量</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="gas">
                        <label class="form-check-label" for="gas1">50cc以下</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="gas">
                        <label class="form-check-label" for="gas2">51cc以上125cc以下</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="gas">
                        <label class="form-check-label" for="gas3">126cc以上400cc以下</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="gas">
                        <label class="form-check-label" for="gas4">401cc以上</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="type" class="control-label col-sm-2">タイプ</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">ネイキッド</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">スパースポーツ</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">オフロード</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">トライク</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">スクーター</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">クルーザー</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">ストリートファイター</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="type">
                        <label class="form-check-label" for="type">ツアラー</label>
                    </div>
                </div>-->
                 <input type="submit" value="検索">
            </form>
        </div>
        <div class="container text-center m-2 pb-2">
           <h2 class="mb-4">バイク一覧画面</h2>
           <div class="row">

           			<c:forEach var="moto" items="${motoList}">
           				<div  class="moto col-sm-3 mr-3">
                    		<a href="MotoDetail?id=${moto.motoId}"><img src="img/${moto.fileName}" width="200" height="200"></a>
                    		<h5>${moto.motoName}</h5>
                    		<p>排気量：${moto.gas}cc</p>
                    		<p>必須免許：${moto.licence}</p>
                    		<p>価格：月${moto.price}円</p>
                    		<p><a href="MotoDetail?id=${moto.motoId}" class="btn btn-outline-info" value="詳細">詳細</a> <a href="OptionList?id=${moto.motoId}" class="btn btn-outline-primary" value="選択">選択</a></p>
                    		<p><a href="#" class="btn btn-outline-danger">非表示</a><p><a href="#" class="btn btn-outline-danger">表示</a><a href="#" class="btn btn-outline-success">削除</a></p>
                		</div>
           			</c:forEach>

          <!--      <div  class="moto col mr-3">
                    <a href="#"><img src="img/monkey_01.jpg" width="200" heiht="200"></a>
                    <h5>モンキー</h5>
                    <p>排気量：50cc</p>
                    <p>必須免許：原付免許以上又は普通自動車免許</p>
                    <p>価格：月33,600円</p>
                    <p><a href="#" class="btn btn-outline-info" value="詳細">詳細</a> <a class="btn btn-outline-primary" value="選択">選択</a></p>
                    <p><a href="#" class="btn btn-outline-danger">非表示</a><p><a href="#" class="btn btn-outline-danger">表示</a><a href="#" class="btn btn-outline-success">削除</a></p>
                </div>
                <div  class="moto col mr-3">
                    <a href="#"><img src="img/yzf-r125.jpg" width="200" heiht="200"></a>
                    <h5>YZF-R125</h5>
                    <p>排気量：125cc</p>
                    <p>必須免許：小型自動二輪免許以上</p>
                    <p>価格：月33,600円</p>
                    <p><a href="#" class="btn btn-outline-info" value="詳細">詳細</a> <a class="btn btn-outline-primary" value="選択">選択</a></p>
                    <p><a href="#" class="btn btn-outline-danger">非表示</a><p><a href="#" class="btn btn-outline-danger">表示</a><a href="#" class="btn btn-outline-success">削除</a></p>
                </div>
                <div  class="moto col">
                    <a href="#"><img src="img/390DUKE.png" width="200" heiht="200"></a>
                    <h5>390DUKE</h5>
                    <p>排気量：390cc</p>
                    <p>必須免許：普通自動二輪免許以上</p>
                    <p>価格：月33,600円</p>
                    <p><a href="#" class="btn btn-outline-info" value="詳細">詳細</a> <a class="btn btn-outline-primary" value="選択">選択</a></p>
                    <p><a href="#" class="btn btn-outline-danger">非表示</a><p><a href="#" class="btn btn-outline-danger">表示</a><a href="#" class="btn btn-outline-success">削除</a></p>
                </div>
            </div>
            <div class="row mt-3">
                <div  class="moto col mr-3">
                    <a href="#"><img src="img/NM4-02.jpg" width="200" heiht="200"></a>
                    <h5>NM4-02</h5>
                      <P>排気量：745cc</P>
                      <p>必須免許：大型自動二輪免許又はAT限定大型自動二輪免許</p>
                      <p>価格：月33,600円</p>
                      <p><a href="#" class="btn btn-outline-info" value="詳細">詳細</a> <a class="btn btn-outline-primary" value="選択">選択</a></p>
                    <p><a href="#" class="btn btn-outline-danger">非表示</a><p><a href="#" class="btn btn-outline-danger">表示</a><a href="#" class="btn btn-outline-success">削除</a></p>
                  </div>
                  <div  class="moto col mr-3">
                      <a href="#"><img src="img/ninjaH2SXSE+.jpg" width="200" height="200"></a>
                      <h5>NINJA H2 SX SE+</h5>
                      <p>排気量：998cc</p>
                      <p>必須免許：大型自動二輪免許</p>
                      <p>価格：月33,600円</p>
                      <p><a href="#" class="btn btn-outline-info" value="詳細">詳細</a> <a class="btn btn-outline-primary" value="選択">選択</a></p>
                      <p><a href="#" class="btn btn-outline-danger">非表示</a><p><a href="#" class="btn btn-outline-danger">表示</a><a href="#" class="btn btn-outline-success">削除</a></p>
                  </div>
                  <div  class="moto col ">
                    <a href="#"><img src="img/can-am-spader-F3.jpg"
                                     width="200" height="200"></a>
                      <h5>カンナム スパイダー F3-S</h5>
                      <p>排気量：1330cc</p>
                      <p>必須免許：普通自動車免許</p>
                      <p>価格：月33,600円</p>
                      <p><a href="#" class="btn btn-outline-info" value="詳細">詳細</a> <a class="btn btn-outline-primary" value="選択">選択</a></p>
                      <p><a href="#" class="btn btn-outline-danger">非表示</a><p><a href="#" class="btn btn-outline-danger">表示</a><a href="#" class="btn btn-outline-success">削除</a></p>
                  </div>
                   -->
              </div>
            </div>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>メニュー</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>