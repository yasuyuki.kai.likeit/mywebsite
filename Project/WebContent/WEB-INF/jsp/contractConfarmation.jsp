<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>契約確認</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>

    </style>
</head>
<body>
    <header class="bg-info pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn btn btn-primary mr-2" value="バイク一覧">バイク一覧</a>
            <a class="btn bg-warning mr-2" value="ログイン">ログイン</a>
            <a class="btn bg-primary mr-2" value="新規登録">新規登録</a>
        </div>
    </header>
    <div class="moto container text-center my-2 p-2">
        <h1 class="mt-2 py-2">${moto.motoName}</h1>
        <img src="img/${moto.fileName}">
        <p>価格：月${moto.price}円</p>
    </div>
    <div class="period bg-success container text-center my-2 p-2">
        <p>レンタル期間：${period.period}</p>
    </div>
    <div class="option-confirmation bg-danger container text-center my-2 p-2">
    	<c:forEach var="option" items="${optionList}">
        	<p>${option.name}</p>
        </c:forEach>
    </div>
    <div class="total-price bg-info container text-center my-2 p-2">
        <p>${moto.motoName}：月 ${moto.price}円</p>
        <p>オプション：月 ${optionTotalPrice.totalPrice}</p>
        <p>保険料：月 50,000円</p>
        <p>総合計額： ${totalPrice}円</p>
        <p>月々の金額：月 ${monthlyPrice}円</p>
    </div>
    <div class="update_date">
                  	<input name="updateDate" type="hidden" value="<%= System.currentTimeMillis() %>">
                </div>
    <div class="confarmation container text-center my-2 p-2">
        <a href="MotoList" class="btn btn-success">リセット</a>
        <a href="ContractConfirm" class="btn btn-primary">確定</a>
    </div>

    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>メニュー</h5>
            <ul>
                <li><a class="text-dark" href="MotoList">バイク一覧</a></li>
                <li><a class="text-dark" href="Login">ログイン</a></li>
                <li><a class="text-dark" href="Signup">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>