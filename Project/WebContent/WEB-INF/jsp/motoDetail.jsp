<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>バイク詳細</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
            <a class="btn bg-warning mr-2" value="ログイン">ログイン</a>
            <a class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
        </div>
    </header>
    <div class="text-center mx-2">
        <div class="container p-3">
            <h1 class="mt-2 py-2">${moto.motoName}</h1>
            <img src="img/${moto.fileName}">
            <p>バイクID：${moto.motoId}</p><!-- 管理者のみ -->
            <P>メーカー：${moto.makerName}</P>
            <p>排気量：${moto.gas}cc</p>
            <p>価格：月${moto.price}円</p>
            <p>必須免許：${moto.licence}</p>
            <p>${moto.explanation}</p>
            <p>
                <form><!--- 管理者のみ -->
                    <a href="#" class="btn">編集</a>
                    <a href="#" class="btn">削除</a>
                    <a href="#" class="btn">非表示</a>
                    <a href="#" class="btn">表示</a>
                </form>
            </p>
        </div>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>