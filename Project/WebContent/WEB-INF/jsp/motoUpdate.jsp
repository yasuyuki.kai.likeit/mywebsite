<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCKTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>バイク情報更新</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style>
        .container{
            width:1000px;
            margin:0 auto;

        }
    </style>
</head>
<body>
    <header class="bg-secondary pb-5 h-3 mb-3">
        <div class="header-left float-left">
            <h5>バイクサブスク</h5>
        </div>
        <div class="header-right float-right">
            <a class="btn bg-warning mr-2" type="button" value="バイク一覧">バイク一覧</a>
            <a class="btn bg-primary mr-2" type="button" value="新規登録">新規登録</a>
        </div>
    </header>
    <div class=" bg-secondary container text-center">
        <h2 class="mb-4">バイク情報更新</h2>
        <form>
            <div class="moto_id mb-4">
                    <span class="mr-4">バイクID</span>
                    <input type="text" value="バイクID" name="moto_id" readonly>
            </div>
            <div class="name mb-4">
                    <span class="mr-4">名前</span>
                    <input type="text" value="DUKE390" name="name">
            </div>
            <div class="img mb-4">
                    <span class="mr-4">写真</span>
                    <input type="file" name="img">
            </div>
            <div class="maker mb-4">
                    <span class="mr-4">メーカー</span>
                    <input type="text" value="KTM" name="maker">
            </div>
            <div class="gas mb-4">
                    <span class="mr-4">排気量</span>
                    <input type="text" value="390" name="gas">
            </div>
            <div class="price mb-4">
                    <span class="mr-4">レンタル価格</span>
                    <input type="text" value="60,000" name="price">
            </div>
            <div class="maker_explain mb-4">
                   <span class="mr-4">メーカー説明</span>
                　　<textarea name="maker_explain" cols="30" rows="2">KTM 390 DUKEは、ストリートモーターサイクルのスリルを表すのに多くの点で最適な例です。コーナーロケットと称されるこのバイクは、楽しさとライダーの価値を最大にし、俊敏なハンドル操作がものをいうときに実力を発揮します。羽のように軽く、パワフルで最新鋭のテクノロジーを詰め込んで、それはあなたが都会のジャングルにを駆け抜けるときも、曲がりくねった森に自分の名前を刻むときも、スリルな乗車を保証します。</textarea>
            </div>
            <div class="licence">
                <span class="mr-4">必須免許証</span>
                <select name="licence">
                    <option value="原付免許">原付免許</option>
                    <option value="小型二輪免許">小型二輪免許</option>
                    <option value="AT限定小型二輪免許">AT限定小型二輪免許</option>
                    <option value="普通自動二輪免許 check">普通自動二輪免許</option>
                    <option value="AT限定普通自動二輪免許">AT限定普通自動二輪免許</option>
                    <option value="大型自動二輪免許">大型自動二輪免許</option>
                    <option value="AT限定大型自動二輪免許">AT限定大型自動二輪免許</option>
                    <option value="普通自動車免許">普通自動車免許</option>
                </select>
            </div>
            <div class="date">
                <input type="hidden" name="createDate">
                <input type="hidden" name="updateDate">
            </div>
            <div class="btn">
                <input class="bg-primary" type="button" value="登録">
            </div>
        </form>
    </div>
    <footer class="bg-warning">
        <div class="container ml-2">
            <h5>バイクサブスク</h5>
            <ul>
                <li><a class="text-dark" href="#">バイク一覧</a></li>
                <li><a class="text-dark" href="#">ログイン</a></li>
                <li><a class="text-dark" href="#">新規登録</a> </li>
            </ul>
        </div>
    </footer>
</body>
</html>