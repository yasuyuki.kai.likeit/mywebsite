package beans;

public class RentalData {
	private int id;
	private String period;

	private int choiceId;
	private int historyId;

	public RentalData(int id, String period) {
		this.id = id;
		this.period = period;
	}
	public RentalData(String period) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.period = period;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public int getChoiceId() {
		return choiceId;
	}
	public void setChoiceId(int choiceId) {
		this.choiceId = choiceId;
	}
	public int getHistoryId() {
		return historyId;
	}
	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

}
