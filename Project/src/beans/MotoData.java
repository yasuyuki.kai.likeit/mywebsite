package beans;

public class MotoData {
	private int motoId;
	private String motoName;
	private int makerId;
	private int price;
	private String fileName;
	private String explanation;
	private int gas;
	private String licence;

	private String makerName;


	public MotoData(int motoId, String motoName, int makerId, int price, String fileName, int gas,String licence) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.motoId = motoId;
		this.motoName = motoName;
		this.makerId = makerId;
		this.price = price;
		this.fileName = fileName;
		this.gas = gas;
		this.licence = licence;
	}
	public MotoData(int motoId, String name, int price, String fileName, String explanation, int gas,
			String licence, String makerName) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.motoId = motoId;
		this.motoName = name;
		this.price = price;
		this.fileName = fileName;
		this.explanation = explanation;
		this.gas = gas;
		this.licence = licence;
		this.makerName = makerName;
	}
	public int getMotoId() {
		return motoId;
	}
	public void setMotoId(int motoId) {
		this.motoId = motoId;
	}
	public String getMotoName() {
		return motoName;
	}
	public void setMotoName(String motoName) {
		this.motoName = motoName;
	}
	public int getMakerId() {
		return makerId;
	}
	public void setMakerId(int makerId) {
		this.makerId = makerId;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	public int getGas() {
		return gas;
	}
	public void setGas(int gas) {
		this.gas = gas;
	}
	public String getLicence() {
		return licence;
	}
	public void setLicence(String licence) {
		this.licence = licence;
	}
	public String getMakerName() {
		return makerName;
	}
	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}


}
