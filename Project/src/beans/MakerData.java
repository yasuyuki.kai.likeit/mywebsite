package beans;

public class MakerData {
	private int id;
	private String name;
	public MakerData(int makerId, String makerName) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = makerId;
		this.name = makerName;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
