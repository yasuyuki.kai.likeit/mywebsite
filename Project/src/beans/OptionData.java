package beans;

public class OptionData {
	private int id;
	private String name;
	private int price;

	private int totalPrice;

	public OptionData(int optionId, String optionName, int price, int totalPrice) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = optionId;
		this.name = optionName;
		this.price = price;
		this.totalPrice = totalPrice;
	}
	public OptionData(int optionId, String optionName, int price) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = optionId;
		this.name = optionName;
		this.price = price;
	}

	public OptionData(int optionId) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = optionId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}





}
