package beans;

public class LicenceData {
	private int id ;
	private String name;

	private int posession_id;
	private int licence_id;
	private int user_id;

	public LicenceData(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public LicenceData(int posession_id, int licence_id, int uesr_id) {
		this.posession_id = posession_id;
		this.licence_id = licence_id;
		this.user_id = user_id;
	}

	public int getPosession_id() {
		return posession_id;
	}
	public void setPosession_id(int posession_id) {
		this.posession_id = posession_id;
	}
	public int getLicence_id() {
		return licence_id;
	}
	public void setLicence_id(int licence_id) {
		this.licence_id = licence_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
