package beans;

import java.util.Date;

public class UserData {
	private int id;
	private String loginId;
	private String name;
	private String password;
	private Date birthDate;
	private String createDate;
	private String updateDate;

	public UserData(int id, String loginId, String name, String password, Date birthDate, String createDate, String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.password = password;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public UserData(int id, String loginId, String name) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.loginId = loginId;
		this.name = name;
	}

	public UserData(String loginId) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginId;
	}

	public UserData(int id) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoinId() {
		return loginId;
	}

	public void setLoinId(String loinId) {
		this.loginId = loinId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}
