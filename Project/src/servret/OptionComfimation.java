package servret;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.OptionData;
import dao.OptionDAO;

/**
 * Servlet implementation class OptionComfimation
 */
@WebServlet("/OptionComfimation")
public class OptionComfimation extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OptionComfimation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String motoId = (String) session.getAttribute("motoId");
		OptionDAO optionDao = new OptionDAO();
		 List<OptionData> optionList = optionDao.findOption(motoId);
		 request.setAttribute("optionList", optionList);

		 List<OptionData> choiceOption = optionDao.choiceOption();
		 request.setAttribute("choiceOption", choiceOption);

		 int totalPrice = 0;



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/optionConfirmation.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String[] select = request.getParameterValues("select");
		String[] delete = request.getParameterValues("delete");

		List<OptionData> optionList = new ArrayList<OptionData>();

		OptionDAO optionDao = new OptionDAO();

		for(int i = 0; i < select.length; i++) {
			optionList.add(optionDao.findByLoinOptionIdInfo(select[i]));
		}

		request.setAttribute("optionList", optionList);

	}

}
