package servret;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.MotoData;
import dao.MotoDAO;

/**
 * Servlet implementation class MotoDetail
 */
@WebServlet("/MotoDetail")
public class MotoDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MotoDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		MotoDAO motoDao = new MotoDAO();
		MotoData moto = motoDao.findByIdInfo(id);
		request.setAttribute("moto", moto);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/motoDetail.jsp");
		dispatcher.forward(request, response);
	}

}
