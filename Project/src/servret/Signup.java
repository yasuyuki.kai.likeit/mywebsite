package servret;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.LicenceData;
import beans.UserData;
import dao.LicenceDAO;
import dao.UserDAO;

/**
 * Servlet implementation class singin
 */
@WebServlet("/Signup")
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Signup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LicenceDAO licenceDao = new LicenceDAO();
		List<LicenceData> licence = licenceDao.findAll();
		request.setAttribute("licenceList", licence);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String[] licence = request.getParameterValues("licence");
		String createDate = request.getParameter("createDate");
		String updateDate = request.getParameter("updateDate");

		if(!(password.equals(rePassword))){
			request.setAttribute("errMsg", "入力された内容が正しくありません1");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(loginId.equals("") || password.equals("") || rePassword.equals("") || name .equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容が正しくありません2");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		UserDAO user = new UserDAO();
		UserData login_id = user.findByLoinIdInfo(loginId);
		if(!(login_id == null)) {
			request.setAttribute("errMsg", "入力された内容が正しくありません3");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		user.insert(loginId, name, birthDate, password);

		int id = user.findByIdinfo(loginId);

		LicenceDAO licenceDao = new LicenceDAO();
		for(int i = 0; i < licence.length; i++) {
			licenceDao.insertLicence(licence[i], id);
		}


		response.sendRedirect("Index");
	}

}
