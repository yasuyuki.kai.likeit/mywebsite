package servret;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.MakerData;
import beans.MotoData;
import dao.MotoDAO;

/**
 * Servlet implementation class MotoList
 */
@WebServlet("/MotoList")
public class MotoList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MotoList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		MotoDAO moto = new MotoDAO();
		List<MotoData> motoList = moto.findAll();

		request.setAttribute("motoList", motoList);
		List<MakerData> makerList = moto.makerFindAll();

		request.setAttribute("makerList", makerList);

		RequestDispatcher disoatcher = request.getRequestDispatcher("/WEB-INF/jsp/motoList.jsp");
		disoatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String makerId = request.getParameter("maker");
		MotoDAO motoDao = new MotoDAO();
		List<MotoData> motoList = motoDao.findMoto(makerId);
		request.setAttribute("motoList", motoList);
		RequestDispatcher disoatcher = request.getRequestDispatcher("/WEB-INF/jsp/motoList.jsp");
		disoatcher.forward(request, response);

	}
}
