package servret;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.OptionData;
import beans.RentalData;
import dao.OptionDAO;
import dao.RentalDAO;

/**
 * Servlet implementation class OptionList
 */
@WebServlet("/OptionList")
public class OptionList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OptionList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");

		HttpSession session = request.getSession();
		session.setAttribute("motoId", id);
		System.out.println("choice!!");

		RentalDAO rentalDao = new RentalDAO();
		List<RentalData> rentalList = rentalDao.findPeriod();
		request.setAttribute("rentalList", rentalList);

		OptionDAO optionDao = new OptionDAO();
		 List<OptionData> optionList = optionDao.findOption(id);
		 request.setAttribute("optionList", optionList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/optionList.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Ato-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		String period = request.getParameter("period");
		String[] option = request.getParameterValues("option");

		if(period == null) {
			request.setAttribute("errMsg", "レンタル期間が選択されていません");
			RentalDAO rentalDao = new RentalDAO();
			List<RentalData> rentalList = rentalDao.findPeriod();
			request.setAttribute("rentalList", rentalList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/optionList.jsp");
			dispatcher.forward(request, response);
			return;
		}
		RentalDAO rentalDao = new RentalDAO();
		RentalData rental = rentalDao.findByRentalInfo(period);

		session.setAttribute("period", rental);
		System.out.println("period choice!!");


		OptionDAO optionDao = new OptionDAO();

		for(int i = 0; i < option.length; i++) {
			optionDao.insertOption(option[i]);
			System.out.println("choice");
		}
		System.out.println("OptionChoice!!");

		response.sendRedirect("ContractComfimation");


	}

}
