package servret;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.MotoData;
import beans.OptionData;
import beans.RentalData;
import dao.MotoDAO;
import dao.OptionDAO;
import dao.RentalDAO;

/**
 * Servlet implementation class ContractComfimation
 */
@WebServlet("/ContractComfimation")
public class ContractComfimation extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContractComfimation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String motoId = (String) session.getAttribute("motoId");
		String periodId = (String) session.getAttribute("period");
		int monthlyPrice = 0;
		int totalPrice = 0;

		RentalDAO retalDao = new RentalDAO();
		RentalData period = RentalDAO.findByRentalInfo(periodId);
		request.setAttribute("period", period);

 		MotoDAO motoDao = new MotoDAO();

		MotoData moto = motoDao.findByIdInfo(motoId);
		request.setAttribute("moto", moto);

		OptionDAO optionDao = new OptionDAO();
		List<OptionData> optionList = optionDao.choiceOption();
		request.setAttribute("optionList", optionList);

		int optionTotalPrice = optionDao.getOptionTotalPrice();
		request.setAttribute("optionTotalPrice", optionTotalPrice);

		monthlyPrice = optionTotalPrice + moto.getPrice() + 50000;
		request.setAttribute("monthlyPrice", monthlyPrice);

		if(periodId.equals("1")) {
			totalPrice = monthlyPrice;
			request.setAttribute("totalPrice", totalPrice);
		}else if(periodId.equals("2")) {
			totalPrice = monthlyPrice * 3;
			request.setAttribute("totalPrice", totalPrice);
		}else {
			totalPrice = monthlyPrice * 6;
			request.setAttribute("totalPrice", totalPrice);
		}

		RequestDispatcher disoatcher = request.getRequestDispatcher("/WEB-INF/jsp/contractConfarmation.jsp");
		disoatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
