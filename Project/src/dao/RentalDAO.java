package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.RentalData;

public class RentalDAO {

	public List<RentalData> findPeriod(){
		Connection con = null;
		List<RentalData> periodList = new ArrayList<RentalData>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from rental";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int rentalId =rs.getInt("rental_id");
				String retalName = rs.getString("rental_p");


				RentalData period = new RentalData(rentalId, retalName);

				periodList.add(period);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return periodList;

	}


	public static RentalData findByRentalInfo(String id) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select rental_p from rental where rental_id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}


			String period = rs.getString("rental_p");
			return new RentalData(period);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}
}
