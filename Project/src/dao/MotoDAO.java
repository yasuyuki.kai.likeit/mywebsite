package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.MakerData;
import beans.MotoData;

public class MotoDAO {

	public List<MotoData> findAll(){
		Connection con = null;
		List<MotoData> motoList = new ArrayList<MotoData>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from moto";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int motoId =rs.getInt("moto_id");
				String motoName = rs.getString("moto_name");
				int makerId = rs.getInt("maker_id");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				int gas = rs.getInt("gas");
				String licence = rs.getString("licence");

				MotoData moto = new MotoData(motoId, motoName, makerId, price, fileName, gas, licence);

				motoList.add(moto);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return motoList;

	}

	public MotoData findByIdInfo(String id) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql ="SELECT m.*, ma.maker_name"
					+" FROM moto m"
					+" INNER JOIN maker ma"
					+" ON m.maker_id = ma.maker_id"
					+" WHERE m.moto_id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			int motoId = rs.getInt("moto_id");
			String name  = rs.getString("moto_name");
			int price  = rs.getInt("price");
			String fileName  = rs.getString("file_name");
			String explanation  = rs.getString("explanation");
			int gas  = rs.getInt("gas");
			String licence  = rs.getString("licence");
			String makerName = rs.getString("maker_name");
			return new MotoData(motoId, name, price, fileName, explanation, gas, licence, makerName);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;


	}
	public List<MotoData> findMoto(String Id){
		Connection con = null;
		List<MotoData> motoList = new ArrayList<MotoData>();
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM moto WHERE maker_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, Id);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int motoId =rs.getInt("moto_id");
				String motoName = rs.getString("moto_name");
				int makerId = rs.getInt("maker_id");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				int gas = rs.getInt("gas");
				String licence = rs.getString("licence");

				MotoData moto = new MotoData(motoId, motoName, makerId, price, fileName, gas, licence);

				motoList.add(moto);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return motoList;

	}

	public List<MakerData> makerFindAll(){
		Connection con = null;
		List<MakerData> makerList = new ArrayList<MakerData>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from maker";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int makerId =rs.getInt("maker_id");
				String makerName = rs.getString("maker_name");


				MakerData maker = new MakerData(makerId, makerName);

				makerList.add(maker);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return makerList;

	}

}
