package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.LicenceData;



public class LicenceDAO {

	public List<LicenceData> findAll(){
		Connection con = null;
		List<LicenceData> licenceList = new ArrayList<LicenceData>();
		try {
			con = DBManager.getConnection();
			String sql = "select * from licence";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while(rs.next()) {
				int id =rs.getInt("licence_id");
				String name = rs.getString("licence_name");


				LicenceData user = new LicenceData(id, name);

				licenceList.add(user);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return licenceList;

	}





	public void insertLicence(String licence, int id) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();
			String insertSql = "INSERT INTO p_licence(licence_id, user_id) VALUES(?,?)";
			pStmt = con.prepareStatement(insertSql);

			pStmt.setString(1, licence);
			pStmt.setInt(2, id);

			pStmt.executeUpdate();
			System.out.println(pStmt);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}



}

