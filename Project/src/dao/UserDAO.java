package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserData;


public class UserDAO {

	public UserData findByLoginInfo(String loginId, String password) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select * from user where login_id = ? and password = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
//			String pass = Encryption(password);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int idData = rs.getInt("user_id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("user_name");
			return new UserData(idData, loginIdData, nameData);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}





	public void insert(String loginId, String name, String birthdate, String password) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();
			String insertSql = "INSERT INTO user(login_id, user_name, birth_date, password, create_date, update_date) VALUES(?,?,?,?,NOW(),NOW())";
			pStmt = con.prepareStatement(insertSql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthdate);
			String pass = Encryption(password);
			pStmt.setString(4, pass);

			pStmt.executeUpdate();
			System.out.println(pStmt);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public UserData findByLoinIdInfo(String loginId) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select * from user where login_id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			return new UserData(loginIdData);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}


	public int findByIdinfo(String loginId) {
		Connection con = null;
		UserData id = null;
		int idData = 0;
			try {
			con = DBManager.getConnection();

			String sql = "select user_id from user where login_id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return 0;
			}
			idData = rs.getInt("user_id");



		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

			return idData;


	}


	public String Encryption(String password) {
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		//標準出力
		System.out.println(result);
		return result;

	}
}
