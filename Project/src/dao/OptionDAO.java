package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.OptionData;

public class OptionDAO {

	public List<OptionData> findOption(String Id){
		Connection con = null;
		List<OptionData> optionList = new ArrayList<OptionData>();
		try {
			con = DBManager.getConnection();
			String sql = "SELECT o.* FROM option_info o"
					+ " INNER JOIN  moto_option m "
					+ " ON o.option_id = m.option_id"
					+ " WHERE moto_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, Id);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int optionId =rs.getInt("option_id");
				String optionName = rs.getString("opsion_name");
				int price = rs.getInt("price");


				OptionData option = new OptionData(optionId, optionName, price);

				optionList.add(option);
			}
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return optionList;

	}


	public void insertOption(String option) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			con = DBManager.getConnection();
			String insertSql = "INSERT INTO c_option(option_id) VALUES (?)";
			pStmt = con.prepareStatement(insertSql);

			pStmt.setString(1, option);

			pStmt.executeUpdate();
			System.out.println(pStmt);

		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (pStmt != null) {
					pStmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		}

	public OptionData findByOptionInfo(String id) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select * from option_info where option_id = ?";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int optionId = rs.getInt("option_id");
			String name = rs.getString("option_name");
			int price = rs.getInt("price");
			return new OptionData(optionId, name, price);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}

	public OptionData findByLoinOptionIdInfo(String optionId) {
		Connection con = null;
		try {
			con = DBManager.getConnection();

			String sql = "select * from c_option where option_id = ? history_id = null";
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, optionId);
			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int optionIdData = rs.getInt("option_id");
			return new OptionData(optionIdData);

		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}

		return null;

	}

	public List<OptionData> choiceOption() {
		Connection con = null;
		List<OptionData> choiceOptionList = new ArrayList<OptionData>();
		try {
			con = DBManager.getConnection();
			String sql = "SELECT c_option(option_id) FROM c_option WHERE history_id = null";
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

				while(rs.next()) {
					int optionId =rs.getInt("option_id");

					OptionData option = new OptionData(optionId);

					choiceOptionList.add(option);
				}
			}catch(SQLException e) {
				e.printStackTrace();
				return null;
			}finally {
				if(con != null) {
					try {
						con.close();
					}catch(SQLException e) {
						e.printStackTrace();
					return null;
					}
				}
			}
			return choiceOptionList;

		}


	public int getOptionTotalPrice() {
		Connection con = null;
		int totalPrice = 0;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT SUM(o.price) FROM option_info o"
					+ " INNER JOIN c_option c"
					+ " ON o.option_id = c.option_id"
					+ " WHERE c.history_id = null";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			while(rs.next()) {
				return 0;
			}
			totalPrice =rs.getInt("price");


		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			if(con != null) {
				try {
					con.close();
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}
		return totalPrice;



		}


	}

//	public void update(String id){
//		Connection con = null;
//		PreparedStatement pStmt = null;
//		try {
//			con = DBManager.getConnection();
//			String sql = "UPDATE  c_option SET option_id = ? WHERE history_id = null";
//
//			pStmt = con.prepareStatement(sql);
//
//			pStmt.setString(1, id);
//
//			pStmt.executeUpdate();
//			System.out.println(pStmt);
//		}catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
//				if (pStmt != null) {
//					pStmt.close();
//				}
//				// コネクションインスタンスがnullでない場合、クローズ処理を実行
//				if (con != null) {
//					con.close();
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
//	}


