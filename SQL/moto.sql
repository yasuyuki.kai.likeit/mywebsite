CREATE DATABASE moto_subsc DEFAULT CHARACTER SET utf8;

USE moto_subsc;

CREATE TABLE user (
user_id int PRIMARY KEY AUTO_INCREMENT, 
login_id varchar(255) UNIQUE NOT NULL, 
user_name varchar(252) NOT NULL,  
birth_date date NOT NULL, 
password varchar(255) NOT NULL,  
create_date datetime NOT NULL, 
update_date datetime NOT NULL);

INSERT INTO user VALUES 
(1, 'admin', '管理者', '1997-11-11', 'password', NOW(), NOW());


CREATE TABLE moto (
moto_id int PRIMARY KEY AUTO_INCREMENT, 
moto_name varchar(30) NOT NULL, 
maker_id int NOT NULL, 
price int NOT NULL, 
file_name varchar(256) NOT NULL, 
explanation text DEFAULT NULL, 
gas int NOT NULL, 
licence varchar(30) NOT NULL); 


INSERT INTO moto VALUES
(1, '390DUKE', 1, 60000, '390DUKE.png', 'KTM 390 DUKEは、ストリートモーターサイクルのスリルを表すのに多くの点で最適な例です。コーナーロケットと称されるこのバイクは、楽しさとライダーの価値を最大にし、俊敏なハンドル操作がものをいうときに実力を発揮します。羽のように軽く、パワフルで最新鋭のテクノロジーを詰め込んで、それはあなたが都会のジャングルにを駆け抜けるときも、曲がりくねった森に自分の名前を刻むときも、スリルな乗車を保証します。',  390, '普通自動二輪');

INSERT INTO moto VALUES
(2, 'NM4-02', 2, 80000, 'NM4-02.jpg', 'このNM4は、「近未来」と「COOL」を開発のテーマに設定し、これまでのモーターサイクルとは一線を画した新感覚で独自のスタイリングを追求したモデルです。デザインは、コンセプトに掲げた「フロントマッシブスタイリング」と、ライダーが潜り込むようにして車両との一体感を重視した「コックピットポジション」を具現化しています。この低く構えたコックピットポジションと、アジャスタブルタイプのバックレストによって、新感覚のライディングフィールを楽しむことができます。また、見やすさと楽しさを表現したインストルメントパネル越しに見える風景は、映画の世界に入り込んだかのようなNM4ならではの世界を提供します。', 745, 'AT限定大型二輪免許以上'), 
(3, 'Monkey', 2, 10000, 'monkey_01.pmg', 'モンキー・50周年スペシャルは、スチール製の前後フェンダーや燃料タンク、ヘッドライトケース、サイドカバーに、仕上げの美しさを追求したクロームメッキを施しました。また、足回りの黒塗装で足元を引き締め、モノトーン仕様のチェック柄シートと合わせてシックな雰囲気を演出しています。さらに、タンクバッジの復刻デザインのウイングマーク、サイドカバーの50周年記念の立体エンブレム、燃料タンク上部やシート後部、メインキーの50周年記念ロゴなど、50年を記念した特別な装備を採用しています。', 50, '原付免許以上'), 
(4, 'YZF-R125', 3, 20000, 'yzf-r125.png', '従来の125ccクラスのミニバイクとは異なるフルサイズのシャシーにより、身長の高い成人男性がクラウチングスタイルを取っても窮屈さを感じさせない、快適なライディングポジションを得ることが可能だ。ヤマハが長年熟成してきた軽量・高剛性なデルタボックス・フレームと、軽量な鋳造アルミニウム製スイングアームの組み合わせは、正統なヤマハ・スーパースポーツのセオリーに則ったもので、このクラスでも最強レベルのハンドリングを実現。フロントカウルにビルトインされたシャープなデザインの二眼ヘッドライト、薄くシャープに切れ上がったテールカウルにはLEDテールライトが組み込まれ、YZF-Rシリーズを継承したアグレッシブなスーパースポーツフォルムを体現している。', 125, '小型二輪免許以上'), 
(5, 'NINJA H2 SX SE+', 5, 80000, 'ninjaH2.png', '目指したもの、それは誰も味わったことのないライディングフィールの実現。Ninja H2 CARBONは「全てを超える」というコンセプトをもとに、最大出力170kW（231PS）を発揮するスーパーチャージドエンジンを搭載しています。エンジンと共に開発した完全自社製のスーパーチャージャーにより、どの回転域からでも強烈に加速する圧倒的なパワーを発揮。さらに燃焼効率を徹底的に追求したことにより、インタークーラーを不要としました。また様々な電子制御システムや、ハイスペックなフットワークの採用、国内仕様としてETC2.0を標準装備。ライディングをサポートする最新技術により、ライダーへ操る悦びを提供します。このスーパースポーツマシンは、川崎重工グループのガスタービンや航空宇宙など様々な事業の技術の結晶であり、カワサキのストリートモデル史上最大のエンジン出力を実現した一台です。', 
998, '大型自動二輪免許'), 
(6, 'Can-am spider F3-S', 6, 90000, 'can-am-spider.png', 'バイクでもない。クルマでもない。カナダから来た3輪バイク！', 1330, '普通自動車免許');


CREATE TABLE maker (
maker_id int PRIMARY KEY AUTO_INCREMENT, 
maker_name varchar(30) NOT NULL); 

INSERT INTO maker VALUES
(1, 'KTM'), 
(2, 'HONDA'), 
(3, 'YAMAHA'), 
(4, 'SUZUKI'), 
(5, 'KAWASAKI'), 
(6, 'BRP');


CREATE TABLE rental(
rental_id int PRIMARY KEY AUTO_INCREMENT, 
rental_p varchar(30)
);

INSERT INTO rental VALUES
(1, '1か月'), 
(2, '3か月'), 
(3, '6か月');


CREATE TABLE hide(
hide_id int PRIMARY KEY AUTO_INCREMENT, 
moto_id int  NOT NULL, 
user_id int  NOT NULL, 
hide_no int DEFAULT 1);


CREATE TABLE r_history(
history_id int PRIMARY KEY AUTO_INCREMENT, 
user_id int  NOT NULL, 
rental_id int  NOT NULL, 
moto_id int NOT NULL, 
retal_d date NOT NULL);



CREATE TABLE licence(
licence_id int PRIMARY KEY AUTO_INCREMENT, 
licence_name varchar(15));

INSERT INTO licence VALUES
(1, '原付免許'), 
(2, '小型自動二輪免許'),
(3, 'AT限定小型自動二輪免許'), 
(4, '中型自動二輪免許'), 
(5, 'AT限定中型自動二輪免許'), 
(6, '大型自動二輪免許'), 
(7, 'AT限定大型自動二輪免許'), 
(8, '普通自動車免許');

CREATE TABLE p_licence(
posession_id int PRIMARY KEY AUTO_INCREMENT, 
licence_id int NOT NULL, 
user_id int NOT NULL);

INSERT INTO p_licence VALUES
(1, 8, 1), 
(2, 4, 1);

CREATE TABLE option_info(
option_id int PRIMARY KEY AUTO_INCREMENT, 
opsion_name varchar(256) NOT NULL, 
price int NOT NULL);


INSERT INTO option_info VALUES
(1, 'マフラー', 5000), 
(2, 'LED', 2000), 
(3, 'ETC', 3000),
(4, 'サイドバッグ', 2000);


CREATE TABLE c_opsion(
choice_id int PRIMARY KEY AUTO_INCREMENT, 
opsion_id int NOT NULL, 
history_id int NOT NULL);

